#!/bin/bash
gitlab_token="_прописать токен доступа"  # аксес токен
gitlab_project_id="прописать id проекта"                   # id проекта
gitlab_user_id=   #указать id пользователя под которым пойдем в gitlab
gitlab_server=  #указать url сервера gitlab
source_branch=("7.9.3" "7.10.0")  #указать ветки источник
target_branch=("7.10.0" "develop") #казать ветки назначения
mr_id_open=$(curl --silent --header "PRIVATE-TOKEN: $gitlab_token" "$gitlab_server/api/v4/projects/$gitlab_project_id/merge_requests?state=opened" | grep -o '"iid":[0-9].[0-9].' | grep -o [0-9].[0-9])
#mr_id_open=$(curl --silent --header "PRIVATE-TOKEN: $gitlab_token" "$gitlab_server/api/v4/projects/$gitlab_project_id/merge_requests?state=opened" | jq -r '.[] | .iid')
curl --silent --request PUT --header "PRIVATE-TOKEN: $gitlab_token" "$gitlab_server/api/v4/projects/$gitlab_project_id/merge_requests/$mr_id_open/merge"

#проходимся по веткам и создаем MR по веткам source и target
for (( i=0; i<${#source_branch[@]}; i++ ))
do
  current_source_branch=${source_branch[i]}
  current_target_branch=${target_branch[i]}
  BODY="{
    \"id\": ${gitlab_project_id},
    \"source_branch\": \"${current_source_branch}\",
    \"target_branch\": \"${current_target_branch}\",
    \"remove_source_branch\": false,
    \"title\": \"Automatic merge from ${current_source_branch} -> ${current_target_branch}\",
    \"assignee_id\":\"${gitlab_user_id}\"
  }";
  create_mr=$(curl --silent --request POST --header "PRIVATE-TOKEN: $gitlab_token" "$gitlab_server/api/v4/projects/$gitlab_project_id/merge_requests" --header "Content-Type: application/json" --data "${BODY}")
  sleep 3
  mr_open=$(curl --silent --header "PRIVATE-TOKEN: $gitlab_token" "$gitlab_server/api/v4/projects/$gitlab_project_id/merge_requests?state=opened" | grep -o '"iid":[0-9].[0-9].' | grep -o [0-9].[0-9])
  #mr_open=$(curl --silent --header "PRIVATE-TOKEN: $gitlab_token" "$gitlab_server/api/v4/projects/$gitlab_project_id/merge_requests?state=opened" | jq -r '.[] | .iid')
  sleep 3
  result_mr=$(curl --silent --request PUT --header "PRIVATE-TOKEN: $gitlab_token" "$gitlab_server/api/v4/projects/$gitlab_project_id/merge_requests/$mr_open/merge")
done

