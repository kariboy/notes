#!/bin/bash
container_ids=$(docker ps -q)
for container_id in $container_ids; do
  docker stop $container_id
done
echo "Все контейнеры Docker остановлены."
