def get_roots_of_quadr_eq(a, b, c):
    res = set()
    d = b*b - 4*a*c
    if d >= 0:
        res.add( (-b + d ** 0.5) / (2 * a) )
        res.add( (-b - d ** 0.5) / (2 * a) )
    return sorted( list(res) )
#==============================================================================
a = float( input('a = ') )
if a == 0:
    print('Это не квадратное уравнение.')
else:
    b = float( input('b = ') )
    c = float( input('c = ') )
    res = get_roots_of_quadr_eq(a,b,c)
    match len(res):
        case 0: print('Уравнение не имеет корней.')
        case 1: print(f'Уравнение имеет один корень: {res[0]:.5f}')
        case 2: print(f'Уравнение имеет два корня: {res[0]:.5f} и {res[1]:.5f}')       